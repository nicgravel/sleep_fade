sleep_fade
==========
TimedFader
Will wait for X minutes before fading the Output

Nicolas Gravel
nicgravel@gmail.com

V. 1.1.0.
May 16, 2015
- Add reset when pressing both buttons

V. 1.0.4
April 23, 2013

Arduino 1.0.4
Based on : http://hlt.media.mit.edu/?p=1695


Based on : http://provideyourown.com/2011/arduino-program-attiny/
- Use Arduino 0.22
- Load ATtiny files 
- Run script from Sketchbook : ArduinoISP to program Arduino board
- Select  Attiny85 1 Mhz board and push thios code

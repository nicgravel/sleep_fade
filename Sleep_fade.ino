/*
TimedFader
Will wait for X minutes before fading the Output

Nicolas Gravel
nicgravel@gmail.com

V. 1.1.0
May 16, 2015

Arduino 1.0.4
Based on : http://hlt.media.mit.edu/?p=1695


Based on : http://provideyourown.com/2011/arduino-program-attiny/
- Use Arduino 0.22
- Load ATtiny files 
- Run script from Sketchbook : ArduinoISP to program Arduino board
- Select  Attiny85 1 Mhz board and push thios code

*/

// constants won't change.
// Use only ATtiny85 or Arduino pins initialisation

// set pin numbers: ATtiny85
const int MainLED =  0;      // Main LED + TIP120 for External light
const int Buzzer = 1;        // Piezo
const int Button1 = 2;       // Main button Start the waiting time before Fading Time
const int StatusLED = 3;     // Heartbeat LED
const int Button2 = 4;       // Start or Reset the Fading Timer.

// Set pin number : Arduino
/*
const int MainLED =  11;     // Main LED + TIP120 for External light
const int Buzzer = 12;       // Piezo
const int Button1 = 7;       // Main button Start the waiting time before Fading Time
const int StatusLED = 13;    // Heartbeat LED
const int Button2 = 8;       // Start or Reset the Fading Timer.
*/
// 5minutes=300, 15 minutes=900, 60 minutes=3600, 90minutes=5400, 120minutes=7200
// variables will change:

int TimeToWait = 10;        // -> How many seconds before the FadeOut will start <- Can be adjust
int TimeFade = 900;         // -> On how many seconds the FadeOut will be        <- Can be adjust

int TimeWait = -5;           // Waiting Timer
int TimeFadeLeft = 0;        // Time Left in the FadeOut process
int Interval = 5000;         // HeartBeat delay
long PreviousMillis = 0;     // will store last time we enter the Loop ( each second)  
int delayButton = 0;         // use for delay after pressing 2 buttons        
int Ledstate = LOW;          // Used for the Hartbeat LED. This help for blink from Mostly ON or OFF

void setup() {
  // initialize the OUTPUT pin:
  pinMode(MainLED, OUTPUT);      
  pinMode(StatusLED,OUTPUT);
  pinMode(Buzzer,OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(Button1, INPUT);
  pinMode(Button2,INPUT);
 //Serial.begin(9600);
}


void loop(){
  // read the state of the pushbutton value:
  
  //Reset if both buttons are press
  if(digitalRead(Button2)==HIGH && digitalRead(Button1)==HIGH)
  {
    TimeWait = -5;
    analogWrite(MainLED,0);
    Ledstate==LOW
    TimeFadeLeft = 0;
    delayButton = 3;//Lock button after both were press
  }


  if(delayButton==0)
  {
    if(digitalRead(Button2)==HIGH) //I have a NC button instead of a NO
    { // Button2 will only reset FadeOut Timer
      TimeWait = -2;
      StartFadeOut();
    }

    if(digitalRead(Button1)==HIGH)
    { //Button1 Start Waiting Time before FadeOut Timer
      TimeWait = TimeToWait;
      digitalWrite(Buzzer,HIGH);
      StartFadeOut();
    }
    else
    { // Turn Buzzer Off
      digitalWrite(Buzzer,LOW);
    }  
  }
  else{
    delayButton--;
  }
  
  unsigned long currentMillis = millis();
  if(currentMillis - PreviousMillis > Interval)
  {
    PreviousMillis = currentMillis;   
    //Serial.println(TimeWait);
    if(Ledstate==LOW)
    { // Blink Status LED. StatusLED will be mostly ON When in Waiting time
      // or mostly off when Fading or when done.
      digitalWrite(StatusLED,HIGH);
      delay(50);
      digitalWrite(StatusLED,LOW);
    }
    else
    {
      digitalWrite(StatusLED,LOW);
      delay(50);
      digitalWrite(StatusLED,HIGH);
    }
    
    if(TimeWait == 0)
    {
      // Mostly Done waiting. Turn StatusLED Off
      Ledstate=(LOW);
      digitalWrite(StatusLED,LOW);
      TimeWait--;
    }
    else if(TimeWait > 0)
    {
      //Waiting ....
      TimeWait--;
    }
    
    else if(TimeWait == -1)
    {
      //Done Waiting. 
      TimeWait--;
      MainFlash();
    }
    else if (TimeWait == -5)
    {
      //Startup State do nothing please
    }
    else
    {
     //Fading OUT Started
     Ledstate=LOW;
     if(TimeFadeLeft > -1)
     {
       int x;
       x = map(TimeFadeLeft, 0, TimeFade, 0, 255); //Map the LED value (0-255) to the TimeFade (0-xx)
       //Serial.print("Fade X now is : ");
       //Serial.println(x);
       //Serial.print("Fade Left is : ");
       //Serial.println(TimeFadeLeft);
       analogWrite(MainLED,x);
       TimeFadeLeft--;
      }
      else
      {
        //All done ! StatusLED blink only every 5 sec.
        Interval=5000;
      }
    }
  }   
}


void StartFadeOut()
{
  //FadeOut initialization
  Ledstate = HIGH;
  TimeFadeLeft=TimeFade;
  analogWrite(MainLED,255);
  digitalWrite(StatusLED,HIGH);
  Interval = 1000;
}

void MainFlash()
{
  // Flash MainLED and Buzzer 3 Times.
  for (int i=0; i <= 2; i++)
  {
    analogWrite(MainLED, 0);   // set the LED OFF
    digitalWrite(Buzzer,HIGH);
    delay(100);              // wait 
    analogWrite(MainLED, 255);    // set the LED ON
    digitalWrite(Buzzer,LOW);
    delay(100);
  }
  //playTone();
}


